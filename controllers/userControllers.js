const User = require("../models/User");
const bcrypt = require("bcryptjs");
const auth = require("../auth");
const Product = require("../models/Product");

//REGISTER USER
module.exports.registerUser = (req, res) => {
    req.body;
    let passwordInput = req.body.password;
    const hashedPW = bcrypt.hashSync(passwordInput, 10);

    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPW,
    });

    User.findOne({ email: req.body.email })
        .then((result) => {
            if (result !== null && result.email === req.body.email) {
                return res.send("Account is already registered. Please log in. ");
            } else {
                newUser.save().then((user) =>
                    res.send({
                        Alert: "Account registered successfully! Hello " + req.body.firstName,
                        Details: user,
                    })
                );
            }
        })
        .catch((error) => res.send(error));
};

//VIEW All Users
module.exports.getAllUsers = (req, res) => {
    User.find({})
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//LOGIN User
module.exports.loginUser = (req, res) => {
    User.findOne({ email: req.body.email })
        .then((foundUser) => {
            if (foundUser === null) {
                return res.send("Login failed. Please try again.");
            } else {
                const isPasswordCorrect = bcrypt.compareSync(
                    req.body.password,
                    foundUser.password
                );

                if (isPasswordCorrect) {
                    return res.send({
                        "Login Successful! Here is your Access Token:": auth.createAccessToken(foundUser),
                    });
                } else {
                    return res.send("Login failed. Please try again.");
                }
            }
        })
        .catch((err) => res.send(err));
};

// Update first name and last name
module.exports.updateUserDetails = (req, res) => {
    let updates = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
    };
    User.findByIdAndUpdate(req.user.id, updates, { new: true })
        .then((updatedUserInfo) =>
            res.send({
                Alert: "Changes Saved!",
                Info: updatedUserInfo,
            })
        )
        .catch((err) => res.send(err));
};

//SET an ADMIN
module.exports.updateAdmin = (req, res) => {
    let updates = {
        isAdmin: true,
    };
    User.findByIdAndUpdate(req.params.id, updates, { new: true })

    .then((updatedAdmin) =>
            res.send({
                Alert: updatedAdmin.firstName + " Successfully set as Admin!",
                Info: updatedAdmin,
            })
        )
        .catch((err) => res.send(err));
};

//Remove an ADMIN
module.exports.removeAsAdmin = (req, res) => {
    let updates = {
        isAdmin: false,
    };
    User.findByIdAndUpdate(req.params.id, updates, { new: true })

    .then((updatedAdmin) =>
            res.send({
                Alert: updatedAdmin.firstName + " Removed as Admin!",
                Info: updatedAdmin,
            })
        )
        .catch((err) => res.send(err));
};

//Make an order
module.exports.makeAPurchase = async(req, res) => {
    if (req.user.isAdmin) {
        return res.send("Your account is an Admin. This action prohibited.");
    }
    //await1
    let isUserUpdated = await User.findById(req.user.id).then((user) => {
        let newPurchase = {
            productId: req.body.productId,
            price: req.body.price,
        };
        user.orders.push(newPurchase);
        user.totalAmount.push(newPurchase.price);

        let sum = user.totalAmount.reduce((prev, current) => prev + current, 0);

        User.findByIdAndUpdate(
                req.user.id, { totalAmountOfOrders: sum }, { new: true }
            )
            .then((user) => user)
            .catch((err) => err.message);

        return user
            .save()
            .then((user) => true)
            .catch((err) => err.message);
    });

    if (isUserUpdated != true) {
        return res.send({ message: isUserUpdated });
    }

    //await2
    let isProductUpdated = await Product.findById(req.body.productId).then(
        (product) => {
            let buyer = {
                userId: req.user.id,
            };
            product.buyers.push(buyer);
            return product
                .save()
                .then((product) => true)
                .catch((err) => err.message);
        }
    );
    if (isProductUpdated != true) {
        return res.send({ message: isProductUpdated });
    }
    if (isUserUpdated && isProductUpdated) {
        return res.send({
            message: "Software added to cart successfully.",
        });
    }
};

// view single user's orders

module.exports.getUserOrders = (req, res) => {
    User.findById(req.user.id)
        .then((result) =>
            res.send({
                firstName: result.firstName,
                lastName: result.lastName,
                totalAmount: result.totalAmount,
                orderID: result.orders[0].productId,
            })
        )
        .catch((err) => res.send(err));
};

//view all orders admin only
module.exports.viewAllProductsAdmin = (req, res) => {
    let viewAllOrders = [];
    Product.find({})
        .then((result) => {
            result.forEach((item) => {
                item.buyers.length !== 0 &&
                    viewAllOrders.push({
                        softwareName: item.softwareName,
                        description: item.description,
                        price: item.price,
                        category: item.category,
                        buyers: item.buyers,
                    });
            });

            res.send({ viewAllOrders });
        })
        .catch((err) => res.send(err));
};