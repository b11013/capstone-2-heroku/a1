const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

const { verify } = auth;
const { verifyAdmin } = auth;

//reg user
router.post("/", userControllers.registerUser);

//get all registered users
router.get("/", userControllers.getAllUsers);

//login user
router.post("/login", userControllers.loginUser);

//update fname and lname (admin only)
router.put("/updateUserDetails", verify, userControllers.updateUserDetails);

//SET ADMIN
router.put(
    "/updateAdmin/:id",
    verify,
    verifyAdmin,
    userControllers.updateAdmin
);

//REMOVE AS ADMIN
router.put(
    "/removeAsAdmin/:id",
    verify,
    verifyAdmin,
    userControllers.removeAsAdmin
);

//make a purchase
router.post("/buy", verify, userControllers.makeAPurchase);

//get single user orders
router.get("/viewUser", verify, userControllers.getUserOrders);

//view all orders (admin)
router.get(
    "/viewAllOrders",
    verify,
    verifyAdmin,
    userControllers.viewAllProductsAdmin
);

module.exports = router;