App:
Software ECommerce API

Desc:
Allows users to purchase to a course.
Allows Admin to perform CRUD operations on courses.
Allows regular users to register.

// USER Model:
// all value in the fields should be required
firstName: String,
lastName: String,
email: String,
password: String,
activeStatus: Boolean,
    default: true
isAdmin: Boolean,
    default: false

//Product Model:
softwareName: String,
description: String,
price: Number,
category: String,
isActive: boolean,
    default: true
createdOn: date

// Order Model

orderInfo: [{
    totalAmount: number,
    purchasedOn: date,
    userID: String,
    productID: String,
    orders: String,
        }]